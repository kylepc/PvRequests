import requests
import time
import datetime
import io
import os
import xml.etree.ElementTree as ET

#REQUIRES: url, headers, xml_doc are correct login versions from PvRequest.dict
#MODIFFIES: PvRequest.dict['query_headers'], PvRequest.dict['user_headers']
#EFFECTS: obtains login cert and adds them to query and user service headers

def get_login_cert(url, headers, xml_doc):

	# obtain login cert
	login_request = make_request(url, headers, xml_doc)
	login_cert = parse(login_request)
	
	# add login cert to headers
	dict['query_headers']['Cookie'] = 'LoginCert=' + login_cert
	dict['user_headers']['Cookie']  = 'LoginCert=' + login_cert


#REQUIRES: url, headers, xml_doc are valid for user service, user_name is valid
# 	username obtained as a command line argument
#MODIFIES: $env:temp
#EFFECTS: inserts username into an xml document intended for user service, 
# 	retrieves the resource code for given user name. Prints message and exits
# 	if user name does not exist. Returns ONLY the resource code as a string

def get_res_code(url, headers, xml_doc, user_name):
	# these three lines of code pop up a lot.. make a function?
	xml_loc  = insert_user_info(xml_doc, usr_name=user_name)
	request  = make_request(url, headers, xml_loc)
	res_code = parse(request, dict['usr_response_tag'])

	# ensure a response is obtained for given username
	if res_code is None:
		print("Username does not exist")
		clean_up()
		exit()

	# pull resource code from response
	prev_char = None
	tmp_str   = ''
	for char in res_code:
		if char.isdigit() and prev_char.isdigit():
			tmp_str += prev_char
		prev_char = char
	tmp_str += prev_char
	res_code = tmp_str

	return res_code


#REQUIRES: xml_list is a list of xml documents to be used as requests to the
# 	query service. code is a valid resource or planning code
#MODIFIES: $env:temp
#EFFECTS: queries planview database with each xml document.
# 	returns events, a list of dicts containing allocation & reservation info

def query(xml_list, res_code=None, plan_code=None):
	if res_code is not None:
		code = res_code
		tags = dict['tags']
	elif plan_code is not None:
		code = plan_code
		tags = dict['plan_tag']
	else:
		print("Error: query() needs either a resource code or")
		print("planning code as input")
		os.exit()
		
	events = []
	for xml_loc in xml_list:
		xml_loc = insert_user_info(xml_loc, res_code=code) 
		request = make_request(dict['query_url'], dict['query_headers'],
				xml_loc)
		response = parse(request, dict['tags'])	
		events.extend(response)

	return events


#REQUIRES:
#MODIFIES:
#EFFECTS:

# could call this within query, right after making initial query for event
# yeah, there is definitely a more streamlined way to just do this in query
def get_description(events, xml_loc):
	for event in events:
		# query expects XML locations to be passed in a list
		xml_list = [xml_loc]
		response = query(xml_list, plan_code=event['PLANNING_CODE'])
		name = response[0]
		event['DESCRIPTION'] = name['DESCRIPTION']


#REQUIRES: events is a list of dicts that contain
# 	individual alloc / reservation information
#MODIFIES: nothing
#EFFECTS: returns a list of dicts formatted to be pushed to google calendar

def create_events(events):
	start_time = datetime.datetime(1, 1, 1).today() # 1's to satisfy ctor
	start_time = start_time.replace(microsecond=0)
	end_time = start_time + datetime.timedelta(hours=1) # makes 1 hour event
	event_list = []

	for event in events:
		description = event['DESCRIPTION']
		del event['DESCRIPTION']

		event_template = {
			'summary': description,
			'start': {
				'dateTime': start_time.isoformat() + '-04:00',
				'timezone': 'America/Detroit',
			},
			'end': {
				'dateTime': end_time.isoformat() + '-04:00',
				'timeZone': 'America/Detroit',
			}
		}	
		string = ''
		for key in event:
			string += key + ': ' + event[key] + '\n'
		event_template['description'] = string
		event_list.append(event_template)
	
	return event_list

"""
User should only need to use functions above this line
"""

#REQUIRES: url is a valid url of a Planview webservice
# 	headers is a dict containing proper headers for that webservice
# 	xml is the file location of an xml document
#MODIFIES: nothing
#EFFECTS: posts a xml request to a Planview webservice

def make_request(url, headers, xml):
	f = open(xml, 'r')
	body = f.read()

	r = requests.post(url, data=body, headers=headers)
	
	# check for error
	if r.status_code != requests.codes.ok:
		r.raise_for_status()

	return r.text


#REQUIRES: xml_text is the raw text response from PvRequest.make_request()
# 	tags is a dict of tags specifying column names and arrays to be returned
# 	as a result
# 	will parse login result if tag is not specified (i.e. 'None')
#MODIFIES: nothing
#EFFECTS: returns a list with a dict for each reservation / allocation

def parse(xml_text, tags=None):
	tree = ET.fromstring(xml_text)

	if tags is None:
		return tree[0][0][0].text # TODO: hey hey hey, it's magic!

	else:
		try:
			col_names = []
			for col in tree.iter(tag=tags['col_tag']):
				for name in col:
					col_names.append(name.text)

			list = []
			for array in tree.iter(tag=tags['array_tag']):
				dict = {}
				for col, elem in zip(col_names, array):
					if elem.text is None:
						dict[col] = 'None'
					else: 
						dict[col] = elem.text 
				list.append(dict)
			return list

		except:
			for item in tree.iter(tag=tags):
				return item.text



#REQUIRES: xml_doc is path to local (./) xml document which ends like:
# 	"resource_code=" (query) or "key://3/" (user service).
# 	Either a resource code or a user name is passed in, and is valid.
#MODIFIES: nothing
#EFFECTS: creates a new dir/file in $env:temp to store xml docs with resource
# 	code or user name appended

def insert_user_info(xml_doc, res_code=None, usr_name=None, plan_code=None):	
	if res_code is not None:
		input = '\'' + str(res_code) + '\''
		tag = dict['sql_tag']
	elif usr_name is not None:
		input = usr_name
		tag = dict['usr_tag']
	elif plan_code is not None:
		input = '\'' + str(plan_code) + '\''
		tag = dict['plan_tag']
	else:
		print("Error: insert_user_info() needs either a resource code or")
		print("a user name or a plan code as input")
		os.exit()

	tree = ET.parse(xml_doc)

	# TODO: for loop. Same thing with parse()!
	for elem in tree.iter(tag):
		elem.text += input # Hacky

	dir_loc  = os.environ['TEMP'] + '/XmlFiles'
	file_loc = os.environ['TEMP'] + '/' + xml_doc

	if not os.path.isdir(dir_loc):
		os.mkdir(dir_loc)

	tree.write(file_loc)

	return file_loc
	

#REQUIRES: Nothing
#MODIFIES: $env:temp\XmlFiles
#EFFECTS: Appends a timestamp (XmlFiles<second>.<microsecond>) to 
# 	$env:temp\XmlFiles to avoid errors creating and writing to files.
# 	Can use to create a log in future.
# 	should always be called if insert_user_info() is called

def clean_up():
	# there is a 10^8 chance two files will be appended w/ same number string
	time = datetime.datetime.now()
	time_string = str(time.second) + '.' + str(time.microsecond)
	old_loc = os.environ['TEMP'] + '/XmlFiles'
	new_loc = os.environ['TEMP'] + '/XmlFiles' + time_string

	if os.path.isdir(old_loc):
		os.rename(old_loc, new_loc)
		return new_loc

"""
A dict containg headers, urls, tags, and other definitions needed to make a
SOAP request against the Planview server
"""
dict = {
# file locations of xml docs
	'login_xml': 'XmlFiles/LoginXmlDoc.txt',
	'user_xml': 'XmlFiles/UserXmlDoc.txt',
	'resrv_query_xml': 'XmlFiles/ReserveQueryXmlDoc.txt',
	'alloc_query_xml': 'XmlFiles/AllocationQueryXmlDoc.txt',
	'descript_query_xml': 'XmlFiles/DescriptionQueryXmlDoc.txt',

# WSDL urls
	'login_url': 'https://ppmnp.its.umich.edu/planview/services/LoginService.svc',
	'user_url': 'https://ppmnp.its.umich.edu/planview/services/UserService.svc',
	'query_url': 'https://ppmnp.its.umich.edu/planview/services/QueryService.svc',


# headers
	'login_headers': {
		'Content-Type': 'text/xml',
		'SOAPAction': 'http://schemas.planview.com/PVE/Core/2008/05/20/ILoginService/Login',
	},

# valid LoginCert needs to be added before a request is made
	'user_headers': {
		'Content-Type': 'text/xml',
		'SOAPAction': 'http://schemas.planview.com/PlanviewEnterprise/Services/UserService/2011/11/01/IUserService/Read',
	},
	'query_headers': {
		'Content-Type': 'text/xml',
		'SOAPAction': 'http://schemas.planview.com/PlanviewEnterprise/Services/QueryService/2012/12/IQueryService/Read',
		},


# xml tag for the login result not needed, might be good style though?
#login_tag = "{http://schemas.planview.com/PVE/Core/2008/05/20}LoginResult"
# for parsing a query result
	'tags': {
	'col_tag': '{http://schemas.planview.com/PlanviewEnterprise/OpenSuite/Dtos/QueryDto/2012/12}ColumnNames',
	'array_tag': '{http://schemas.microsoft.com/2003/10/Serialization/Arrays}ArrayOfanyType',
	},
# non-query tags
	'sql_tag': '{http://schemas.planview.com/PlanviewEnterprise/Services/QueryService/2012/12}sqlQuery',
	'usr_tag': '{http://schemas.microsoft.com/2003/10/Serialization/Arrays}string',
	'usr_response_tag': '{http://schemas.planview.com/PlanviewEnterprise/OpenSuite/Dtos/UserDto/2013/01}ResourceKey',
	'plan_tag': '{http://schemas.microsoft.com/2003/10/Serialization/Arrays}anytype i:type={f:string} xmlns:f={http://www.w3.org/2001/XMLSchema}'
}
