It is written with Python 3.5.1. and has not been tested with earlier versions.
To ensure no compatability issues you should use this version or later.

PvRequests was build in PowerShell version 3. 
Compatability with other versions is not garunteed.

PvRequests uses the requests module, which will need to be installed before use.
This module can be installed with:
> pip install requests

