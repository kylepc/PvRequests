#import PvRequest

# url for the login WSDL
login_url = 'https://ppmnp.its.umich.edu/planview/services/LoginService.svc'
# required headers
login_headers = {
		'Content-Type': 'text/xml',
		'SOAPAction': 'http://schemas.planview.com/PVE/Core/2008/05/20/ILoginService/Login'
		}
# xml document is located at ./XmlFiles/LoginXmlDoc.txt
login_xml = 'XmlFiles/LoginXmlDoc.txt'
# xml tag for the login result not needed, might be good style though?
#login_tag = "{http://schemas.planview.com/PVE/Core/2008/05/20}LoginResult"


query_url = 'https://ppmnp.its.umich.edu/planview/services/QueryService.svc?wsdl'

# valid LoginCert needs to be added before a request is made
query_headers = {
		'Content-Type': 'text/xml',
		'SOAPAction': 'http://schemas.planview.com/PlanviewEnterprise/Services/QueryService/2012/12/IQueryService/Read'
		}
reserve_query_xml = 'XmlFiles/ReserveQueryXmlDoc.txt'
allocation_query_xml = 'XmlFiles/AllocationQueryXmlDoc.txt'

tags = {
	'col_tag': '{http://schemas.planview.com/PlanviewEnterprise/OpenSuite/Dtos/QueryDto/2012/12}ColumnNames',
	'array_tag': '{http://schemas.microsoft.com/2003/10/Serialization/Arrays}ArrayOfanyType'
	}

# obtain login cert
login_request = PvRequest.make_request(login_url, login_headers, login_xml)
login_cert = PvRequest.parse_xml(login_request)

query_headers['Cookie'] = 'LoginCert=' + login_cert

# reserve table request
# find more appropriate name, it's xml text
reserve_request = PvRequest.make_request(query_url, query_headers, reserve_query_xml)
reserve_response = PvRequest.parse_xml(reserve_request, tags)
print(reserve_response)

allocation_request = PvRequest.make_request(query_url, query_headers, allocation_query_xml)
allocation_response = PvRequest.parse_xml(allocation_request, tags)
print(allocation_response)
